该三方开源库从github fork过来，主要将底层接口调用的实现修改成鸿蒙接口的实现，将三方库鸿蒙化，供开发鸿蒙应用的开发者使用


fork版本号/日期：1.0 / 2017/3/29


# ButtonProgressBar

ButtonProgressBar一个下载按钮进度条

原项目Readme地址：https://github.com/ishaan1995/ButtonProgressBar/blob/master/README.md

项目移植状态：支持组件所有基本功能

完成度：100%

调用差异：无





# ButtonProgressBar

ButtonProgressBar一个下载按钮进度条


**导入方法**

1.har导入

将har包放入lib文件夹并在build.gradle添加

```
implementation fileTree(dir: 'libs', include: ['*.har'])
```

2.Library引用

添加本工程中UcropLib模块到任意工程中，在需要使用的模块build.gradle中添加

```
implementation project(':buttonprogressbar')
```

or

```
allprojects{
    repositories{
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:ButtonProgressBar:1.0.1'
```

**使用方法**
1 .在布局文件中添加以下代码
```
    <github.ishaan.buttonprogressbar.ButtonProgressBar
            ohos:id="$+id:bpb_main"
            ohos:height="50vp"
            ohos:width="match_parent"
            ohos:bgColor="#ff0000ff"
            ohos:buttonText="Upload"
            ohos:margin="20vp"
            ohos:progColor="#ff00ff00"
            ohos:textColor="#ffffff00"
            ohos:textSize="20fp"
            ohos:type="0"
    />
```
2.在代码中初始化参数
```
// 初始化
buttonProgressBar = (ButtonProgressBar) findComponentById(ResourceTable.Id_bpb_main);
// 设置点击监听
buttonProgressBar.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (buttonProgressBar.getLoaderType() == ButtonProgressBar.Type.DETERMINATE) {
                    callHandler();
                } else {
                    // 开始动画
                    buttonProgressBar.startLoader();
                    eventHandler.postTask(new Runnable() {
                        @Override
                        public void run() {
                            // 结束动画
                            buttonProgressBar.stopLoader();
                            new ToastDialog(getContext()).setText("Complete").show();
                        }
                    }, 5000);
                }
            }
        });
```
3.一些功能设置介绍
```
//设置字体颜色
buttonProgressBar.setTextColor(int textColor)
//设置字体大小
buttonProgressBar.setTextSize(int size)
//设置背景的颜色
buttonProgressBar.setBackgroundColor(int bgColor)
//设置进度条的颜色
buttonProgressBar.setProgressColor(int progColor)
//设置进度条加载的类型
buttonProgressBar.setLoaderType(ButtonProgressBar.Type mLoaderType)
//设置当前的进度
buttonProgressBar.setProgress(int currentProgress)
```
    
