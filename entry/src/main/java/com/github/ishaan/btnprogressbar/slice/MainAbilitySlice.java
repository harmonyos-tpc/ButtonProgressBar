/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.ishaan.btnprogressbar.slice;

import com.github.ishaan.btnprogressbar.ResourceTable;
import github.ishaan.buttonprogressbar.ButtonProgressBar;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.window.dialog.IDialog;
import ohos.agp.window.dialog.ListDialog;
import ohos.agp.window.dialog.ToastDialog;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

/**
 * Main ability slice
 */
public class MainAbilitySlice extends AbilitySlice {
    /**
     * * The constant ITEMS
     */
    private static final String[] ITEMS = {"DETERMINATE", "INDETERMINATE"};
    /**
     * The constant Button progress bar
     */
    private ButtonProgressBar buttonProgressBar;
    /**
     * The constant Event handler
     */
    private EventHandler eventHandler = new EventHandler(EventRunner.getMainEventRunner());
    /**
     * The constant Progress
     */
    private int progress = 0;
    /**
     * The constant Select id
     */
    private int selectId = 0;

    /**
     * On start *
     *
     * @param intent intent
     */
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        buttonProgressBar = (ButtonProgressBar) findComponentById(ResourceTable.Id_bpb_main);
        buttonProgressBar.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (buttonProgressBar.getLoaderType() == ButtonProgressBar.Type.DETERMINATE) {
                    callHandler();
                } else {
                    buttonProgressBar.startLoader();
                    eventHandler.postTask(new Runnable() {
                        @Override
                        public void run() {
                            buttonProgressBar.stopLoader();
                            new ToastDialog(getContext()).setText("Complete").show();
                        }
                    }, 5000);
                }
            }
        });
        findComponentById(ResourceTable.Id_btn_cahnge_mode).setClickedListener(this::onClick);
    }

    /**
     * On click *
     *
     * @param component component
     */
    private void onClick(Component component) {
        buttonProgressBar.reset();
        ListDialog lDialog = new ListDialog(getContext(), ListDialog.SINGLE);
        lDialog.setTitleText("Select Mode");
        lDialog.setSingleSelectItems(ITEMS, selectId); // 0 表示默认被选中的是 index = 0 的这一项
        lDialog.setOnSingleSelectListener((iDialog, position) -> {
            selectId = position;
            switch (position) {
                case 0:
                    buttonProgressBar.setLoaderType(ButtonProgressBar.Type.DETERMINATE);
                    break;
                case 1:
                    buttonProgressBar.setLoaderType(ButtonProgressBar.Type.INDETERMINATE);
                    break;
            }
        });
        lDialog.setButton(0, "取消", new IDialog.ClickedListener() { // 0 表示处于开始位置的按钮，1为中间，2为末尾
            @Override
            public void onClick(IDialog iDialog, int position) {
                lDialog.destroy();
            }
        });
        lDialog.setButton(2, "确定", new IDialog.ClickedListener() {
            @Override
            public void onClick(IDialog iDialog, int position) {
                lDialog.destroy();
            }
        });
        lDialog.show();
    }

    /**
     * Call handler
     */
    private void callHandler() {
        eventHandler.postTask(new Runnable() {
            @Override
            public void run() {
                if (progress <= 100) {
                    updateUI();
                    progress++;
                    callHandler();
                } else {
                    progress = 0;
                    new ToastDialog(getContext()).setText("Complete").show();
                }
            }
        }, 20);
    }

    /**
     * Update ui
     */
    public void updateUI() {
        eventHandler.postTask(new Runnable() {
            @Override
            public void run() {
                buttonProgressBar.setProgress(progress);
            }
        });
    }

    /**
     * On active
     */
    @Override
    public void onActive() {
        super.onActive();
    }

    /**
     * On foreground *
     *
     * @param intent intent
     */
    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
