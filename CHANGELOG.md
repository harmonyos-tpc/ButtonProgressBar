1.0.0

Initial Release

Support Features:
--------------------------------------------
1) 设置字体颜色
ButtonProgressBar.setTextColor(int textColor)
1) 设置字体大小
ButtonProgressBar.setTextSize(int size)
1) 设置背景的颜色
ButtonProgressBar.setBackgroundColor(int bgColor)
1) 设置进度条的颜色
ButtonProgressBar.setProgressColor(int progColor)
1) 设置进度条加载的类型
ButtonProgressBar.setLoaderType(ButtonProgressBar.Type mLoaderType)
1) 设置当前的进度
ButtonProgressBar.setProgress(int currentProgress)

Not Support Features:
--------------------------------------------------------

无